puts `clear`
puts '###MUSIQUITA###'
puts "###ESCOGE LA RUTA DE LA CARPETA CON LA CUAL QUIERES TRABAJAR###\n\n\n"
basedir = gets.strip
hMetadata = {}
hFiles = {}

def menuTags()
  fase = 0
  userValues = []
  loop do 
    puts "Que tags usaras para renombrar la carpeta base?
    1) Artist
    2) Album
    3) Year
    4) Genre"
    user_input = gets.strip.to_i
    fase +=1
    case user_input
    when 1
      userValues.push(user_input)
    when 2
      userValues.push(user_input)
    when 3
      userValues.push(user_input)
    when 4
      userValues.push(user_input)
    else
      p "Opcion no valida"
    end
  if fase == 2
    break
  end
 end
 return (userValues)
end

if basedir == ''
  puts 'DEBES PONER UNA DIRECCION'
else
  Dir.chdir(basedir)
  puts `clear`
  puts "EL ARCHIVO CON EL QUE SE TRABAJARA SERA: #{Dir.pwd}\n\n"
  puts "\n\n###ESCOGE LA EXTENSION DE ARCHIVOS CON LA QUE QUIERES TRABAJAR###\n\n"
  exten = gets.strip
  files_exten = '*.' + exten
end

if files_exten == ''
  puts 'DEBES PONER UNA EXTENSION'
else
  puts `clear`
  found_elements = Dir.glob(files_exten)
  hFiles = {}
  if found_elements.empty?
    puts "no encontre ninguna wea xdxdxdxd"
  else 
    puts "Estos son las canciones que encontre:"
    found_elements.each do |i|
      f = File.open(i)
      f.seek(-128, IO::SEEK_END)
      mp3 = f.read
      title, artist, album, year, comment, genre = mp3.unpack('xxxA30A30A30A4A30C1')
      hMetadata = {:title => title, :artist => artist, :album => album, :year => year, :comment => comment, :genre => genre}
      hFiles[i] = hMetadata
    end
  end
end

options = menuTags()

m = options.map{|e| a = hMetadata.values[e]}

oldBasePath = File.dirname(basedir)
actualDir = File.basename(basedir)
File.rename(basedir,oldBasePath + "/#{m[0]} - #{m[1]}")
Dir.chdir(oldBasePath + "/#{m[0]} - #{m[1]}")
puts Dir.pwd
